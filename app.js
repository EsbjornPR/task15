
let result = getNewValue();
let myGuess = false;
const response = document.getElementById('my-result');
const guess = document.getElementById('my-guess');

response.innerText = 'Waiting for your guess!'

function getNewValue() {
    return Math.floor(Math.random() * Math.floor(100));
  }

function checkResponse(lastGuess) {
    if ( lastGuess < result ) {
        response.innerText = 'Damn! Your guess was to low! Try again.';   
    } else if ( lastGuess > result ) {
        response.innerText = 'Damn! Your guess was to high! Try again.';   
    } else {
        response.innerText = 'Gongratulations you got it right!'; 
    }
}

// EventListener for the guess-button
const guessBtn = document.getElementById('my-btn');
guessBtn.addEventListener('click',  function(){
    // this will refer to the button being clicked
    checkResponse(guess.value);   
})

// EventListener for the reset-button
const resetBtn = document.getElementById('reset-btn');
resetBtn.addEventListener('click',  function(){
    result = getNewValue(); 
    response.innerText = 'Waiting for your guess'; 
    console.log(result); // Don´t sneak peek in the console 
})

  